# Web Volume Meter
This is a simple example of a volume meter using the `AudioContext` native API 
This **does not capture and audio output** it simply analyzes the ongoing stream.

Displays a green Volume bar for the current mic input.
Displays a the volume bar in red if the sound is clipping. 

This is a franken example bashed together and improved slightly from 2 repos 

The `volume-meter.js` code is cleaned up code from 
  * https://github.com/cwilso/volume-meter/

Parts of `app.js` are from 
  * https://github.com/mdn/voice-change-o-matic

