let heading = document.querySelector("h1");
heading.textContent = "CLICK ANYWHERE TO START";

document.body.onclick = init;

function init() {
  document.body.onclick = undefined;
  heading.textContent = "Volume recorder";

  // Updates navigator.mediaDevices.getUserMedia to a promise that will reject if the browser
  // doesn't support the media device
  verifyGetUserMediaExists();

  //main block for doing the audio recording
  navigator.mediaDevices
    .getUserMedia({ audio: true })
    .then(measureVolume)
    .catch(err => console.error("The following gUM error occured: " + err));


  // set up forked web audio context, for multiple browsers
  // window. is needed otherwise Safari explodes
  /** @type {AudioContext} */
  const audioContext = new (window.AudioContext || window.webkitAudioContext)();

  // Has to live outside of measeure volume - reasons yet unknown to me (O.B. 03/08/19)
  let meter;

  /** @param {MediaStream} stream */
  function measureVolume(stream) {
    // Create an AudioNode from the stream.
    const mediaStreamSource = audioContext.createMediaStreamSource(stream);

    // Create a new volume meter and connect it.
    meter = createAudioMeter(audioContext);
    mediaStreamSource.connect(meter);

    // kick off the visual updating
    drawLoop();
  }


  /** @type {CanvasContext} */
  const canvasContext = document.getElementById("meter").getContext("2d");
  const volumeNumber = document.getElementById("volume-number");

  // Time gets passed into this drawLoop function .. not sure from where yet
  function drawLoop(time) {
    const CANVAS_WIDTH = 500;
    const CANVAS_HEIGHT = 50;

    // clear the background
    canvasContext.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

    // check if we're currently clipping
    if (meter.checkClipping()) {
      canvasContext.fillStyle = "red";
    } else {
      canvasContext.fillStyle = "green";
    }

    // draw a bar based on the current volume
    canvasContext.fillRect(0, 0, meter.volume * CANVAS_WIDTH * 1.4, CANVAS_HEIGHT);
    
    volumeNumber.textContent = "Volume " + parseInt(meter.volume * 100);


    // set up the next visual callback
    rafID = window.requestAnimationFrame(drawLoop);
  }
}

// Checs that the `navigator.mediaDevices.getUserMedia` exists otherwise sets a rejected promise for 
//  the user media privilage.
function verifyGetUserMediaExists() {
  // Older browsers might not implement mediaDevices at all, so we set an empty object first
  if (navigator.mediaDevices === undefined) {
    navigator.mediaDevices = {};
  }

  // Some browsers partially implement mediaDevices. We can't just assign an object
  // with getUserMedia as it would overwrite existing properties.
  // Here, we will just add the getUserMedia property if it's missing.
  if (navigator.mediaDevices.getUserMedia === undefined) {
    navigator.mediaDevices.getUserMedia = function (constraints) {
      // First get ahold of the legacy getUserMedia, if present
      const getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
      // Some browsers just don't implement it - return a rejected promise with an error
      // to keep a consistent interface
      if (!getUserMedia) {
        return Promise.reject(new Error("getUserMedia is not implemented in this browser"));
      }
      // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
      return new Promise(function (resolve, reject) {
        getUserMedia.call(navigator, constraints, resolve, reject);
      });
    };
  }
}

